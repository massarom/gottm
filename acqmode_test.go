package gottm_test

import (
	"bytes"
	"encoding/base64"
	"errors"
	"testing"

	"bitbucket.org/massarom/gottm"
)

var (
	goodHeader  = []byte("VFRNOERhdGEBBLXUgQAAAAAAiAC0HwAAAAAYJ/j/")
	badHeader   = []byte("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF/j/")
	shortHeader = string("TQ==")
)

func TestShortHeader(t *testing.T) {
	// Check a header too short
	headerS := make([]byte, 1)
	headerS, ok := base64.StdEncoding.DecodeString(shortHeader)
	_, ok = gottm.NewAcquisitionModeInfo(bytes.NewReader(headerS))
	if ok == nil {
		t.Error("Short header did not cause an error")
	}

}
func TestAcqModeI64f(t *testing.T) {
	// Check first the good header
	headerG := make([]byte, 30)
	n, ok := base64.StdEncoding.Decode(headerG, goodHeader)
	if ok != nil {
		t.Error(ok)
	} else if n == 0 {
		t.Error(errors.New("Nothing was decoded"))
	}
	am, ok := gottm.NewAcquisitionModeInfo(bytes.NewReader(headerG))
	if ok != nil {
		t.Error(ok)
	} else if am.ClkPeriod != gottm.ClkPeriodI64f {
		t.Errorf("Expected clk period %f but got %f", gottm.ClkPeriodI64f, am.ClkPeriod)
	} else if am.Mode != gottm.I64f {
		t.Errorf("Expected Mode %d but got %d", gottm.I64f, am.Mode)
	} else if am.TagByteSize != gottm.TagByteSizeI64f {
		t.Errorf("Expected tag byte size %d but got %d", gottm.TagByteSizeI64f, am.TagByteSize)
	} else if am.Mode.String() != "I-Mode, 64bit, flat" {
		t.Errorf("String representation was wrong. Expecting\n\t%s\nbut got\n\t%s", "I-Mode, 64bit, flat", am.Mode.String())
	}

	// Then check a bad header
	headerB := make([]byte, 30)
	n, ok = base64.StdEncoding.Decode(headerB, badHeader)
	if ok != nil {
		t.Error(ok)
	} else if n == 0 {
		t.Error(errors.New("Nothing was decoded"))
	}
	am, ok = gottm.NewAcquisitionModeInfo(bytes.NewReader(headerB))
	if ok == nil {
		t.Error("Bad header was recognized as a good I64f header")
	}
}
