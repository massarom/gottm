package gottm_test

import (
	"bytes"
	"testing"

	"bitbucket.org/massarom/gottm"
)

const testTagUint64 = uint64(0x0F00)

var testTag = []byte{
	byte((testTagUint64 >> (0 * 8)) & 0xFF), byte((testTagUint64 >> (1 * 8)) & 0xFF), byte((testTagUint64 >> (2 * 8)) & 0xFF), byte((testTagUint64 >> (3 * 8)) & 0xFF),
	byte((testTagUint64 >> (4 * 8)) & 0xFF), byte((testTagUint64 >> (5 * 8)) & 0xFF), byte((testTagUint64 >> (6 * 8)) & 0xFF), byte((testTagUint64 >> (7 * 8)) & 0xFF),
}

func TestRawTag(t *testing.T) {
	r := gottm.NewRawTag(testTag)
	if test := r.Uint64() >> 8; test != (testTagUint64 >> 8) {
		t.Errorf("Shifting went wrong. Expecting %X and got %X", testTagUint64>>8, test)
	}
	if bytes.Compare(r.Bytes(), testTag) != 0 {
		t.Errorf("Read wrong bytes. Expecting %X and got %X", testTag, r.Bytes())
	}
}

func TestTagI64f(t *testing.T) {
	r := gottm.NewRawTag(testTag)
	i64 := gottm.NewTagI64f(r)
	if i64.Channel() != 1 {
		t.Errorf("Channel was %d, not 1.", i64.Channel())
	}
	if i64.Slope() != 0 {
		t.Errorf("Channel was %d, not 0.", i64.Slope())
	}
	if i64.Timestamp() != 0x0F00 {
		t.Errorf("Channel was %d, not 16.", i64.Timestamp())
	}
}
