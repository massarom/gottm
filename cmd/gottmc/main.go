package main

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/massarom/gottm"
	log "github.com/sirupsen/logrus"
)

const usage = `
ttmc counts the number of coincidence events in among the channels provided in the file supplied,
after applying any deadtime settings. Deadtimes MUST be given in a sequential order. E.g., to
put a 60ns deadtime on channel 2 and 4, the correct command-line string is
	ttms -d 0 60e-9 0 60e-9
where we gave channel 1 and 3 a 0s deadtime (just as a placeholder).

Options:
`

// Define new types that implement the flag.Value interface
type channelFlags []uint8

func (f *channelFlags) String() string {
	return fmt.Sprintf("%d", *f)
}

func (f *channelFlags) Set(value string) error {
	valuesStr := strings.Split(value, ",")
	for _, val := range valuesStr {
		val, ok := strconv.Atoi(val)
		if ok == nil {
			if 0 < val && val < 9 {
				*f = append(*f, uint8(val))
			} else {
				ok = gottm.ErrInvalidChannel
			}
		}
		*f = append(*f, uint8(val))
	}
	return nil
}

type floatFlags []float64

func (d *floatFlags) String() string {
	return fmt.Sprintf("%f", *d)
}

func (d *floatFlags) Set(value string) error {
	valuesStr := strings.Split(value, ",")
	for _, v := range valuesStr {
		v, ok := strconv.ParseFloat(v, 64)
		if ok != nil {
			log.Fatal("Error while parsing float type from command line")
		}
		*d = append(*d, v)
	}
	return nil
}

func main() {
	var autoOffsets bool
	var filePath string
	var channels channelFlags
	var trigger uint8
	var deadtimes floatFlags
	var offsets floatFlags
	var window float64

	flag.BoolVar(&autoOffsets, "a", false, "If set, offsets will be analyzed automagically")
	flag.BoolVar(&autoOffsets, "auto", false, "Alias of '-a'")

	flag.Var(&channels, "c", "List of channels among which to calculate the coincidences")
	flag.Var(&channels, "channels", "Alias of '-c'")

	flag.Var(&deadtimes, "d", "Deadtimes, in seconds, to apply to each channel sequentially (i.e., first given deadtime to first channel, etc.)")
	flag.Var(&deadtimes, "deadtimes", "Alais of '-d")

	flag.StringVar(&filePath, "f", "", "Path to the file to analyse")
	flag.StringVar(&filePath, "file", "", "Alias of -f")

	flag.Var(&offsets, "o", "Offsets, in seconds, to apply to each channel sequentially (i.e., first given deadtime to first channel, etc.)")
	flag.Var(&offsets, "offsets", "Alias of '-o'")

	flag.Float64Var(&window, "w", 5e-9, "Coincidence window width in seconds")
	flag.Float64Var(&window, "window", 5e-9, "Alias of '-w'")

	flag.Parse()

	logger := log.WithFields(log.Fields{"filePath": filePath, "window": window})
	if filePath == "" {
		fmt.Print(usage)
		flag.PrintDefaults()
		logger.Fatal("No file was given")
	}

	b, ok := gottm.NewBinFile(filePath)
	defer b.Close()

	if ok != nil {
		logger.Fatal("File could not be opened")
	}

	if channels == nil {
		logger.Fatal("No channels were given for coincidence calculations")
	}

	logger.Info("Setting coincidence window")
	b.SetWindow(window)

	if autoOffsets {
		logger.Info("Automatically searching for offsets")
		logger = logger.WithField("trigger", trigger)
		if trigger == 0 {
			logger.Data["trigger"] = channels[0]
			logger.Warning("No trigger channel set, using first coincidence channels")
		} else {
			logger.Info("Using user-defined trigger channel")
			b.SetTrigger(trigger)
		}
	} else if offsets != nil {
		logger.WithField("offsets", offsets).Info("Setting desired offsets")
		b.SetOffsets(offsets...)
	} else {
		logger.Warning("No offests were set")
	}
	if deadtimes == nil {
		logger.Warning("No deadtimes were set")
	} else {
		logger.WithField("deadtimes", deadtimes).Info("Setting desired deadtimes")
		b.SetDeadtimes(deadtimes...)
	}

	fmt.Println(b.Coincidences(channels...))
}
