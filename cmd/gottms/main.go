package main

import (
	"flag"
	"fmt"
	"strconv"

	"bitbucket.org/massarom/gottm"
	log "github.com/sirupsen/logrus"
)

const usage = `
ttms counts the total number of single events in each channel in the file supplied,
after applying any deadtime settings. Deadtimes MUST be given in a sequential order. E.g., to
put a 60ns deadtime on channel 2 and 4, the correct command-line string is
	ttms -d 0 60e-9 0 60e-9
where we gave channel 1 and 3 a 0s deadtime (just as a placeholder).

Options:
`

// Define new types that implement the flag.Value interface
type deadtimesFlags []float64

func (d *deadtimesFlags) String() string {
	return fmt.Sprintf("%f", *d)
}

func (d *deadtimesFlags) Set(value string) error {
	val, ok := strconv.ParseFloat(value, 64)
	if ok == nil {
		*d = append(*d, val)
	}
	return ok
}

func main() {
	var filePath string
	var deadtimes deadtimesFlags

	flag.StringVar(&filePath, "f", "", "Path to the file to analyse")
	flag.StringVar(&filePath, "file", "", "Path to the file to analyse (alias)")

	flag.Var(&deadtimes, "d", "Deadtimes to apply to each channel sequentially (i.e., first given deadtime to first channel, etc.)")
	flag.Var(&deadtimes, "deadtimes", "Deadtimes to apply to each channel sequentially (i.e., first given deadtime to first channel, etc.) (alias)")

	flag.Parse()

	if filePath == "" {
		fmt.Print(usage)
		flag.PrintDefaults()
		log.Fatal("No file was given")
	}

	logger := log.WithField("filePath", filePath)

	b, ok := gottm.NewBinFile(filePath)
	defer b.Close()

	if ok != nil {
		logger.Fatal("File could not be opened")
	}

	if deadtimes == nil {
		logger.Warning("No deadtimes given")
	} else {
		logger.WithField("deadtimes", deadtimes).Info("Setting desired deadtimes")
		b.SetDeadtimes(deadtimes...)
	}

	if n, ok := b.Singles(); ok == nil {
		for _, s := range n {
			fmt.Println(s)
		}
	} else {
		fmt.Println(ok)
	}
}
