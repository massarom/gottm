package gottm

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"math"
	"os"

	log "github.com/sirupsen/logrus"
)

// BinFile represents a binary file created byt the AIT TTM8000 and implements most of the expected
// methods to act on the file.
type BinFile struct {
	ModeInfo      *AcquisitionModeInfo
	fileRef       *os.File
	tagReader     *bufio.Reader
	offsets       []int64
	deadtimes     []uint64
	triggerCh     uint8
	window        uint64
	lastTimestamp []uint64
}

// ErrInvalidChannel indicates that the given channel is not in the range [1..8]
var ErrInvalidChannel = errors.New("given channel is invalid")

// ErrNegativeDeadtime indicates that a deadtime was given a negative value, which is impossible
var ErrNegativeDeadtime = errors.New("deadtimes cannot be negative")

// NewBinFile initializes a new BinFile from a file, whose path is given as argument, and parsing
// the acuisition mode. Deadtimes and offsets are zeroed.
func NewBinFile(path string) (*BinFile, error) {
	fr, ok := os.Open(path)
	if ok != nil {
		return nil, ok
	}
	rd := bufio.NewReaderSize(fr, 30)
	am, ok := NewAcquisitionModeInfo(rd)
	if ok != nil {
		return nil, ok
	}
	rd = bufio.NewReaderSize(fr, 8*1024*1024)
	log.WithField("acqmode", am.Mode.String()).Info("Acquisition mode retrieved")
	b := BinFile{
		ModeInfo: am, fileRef: fr, tagReader: rd,
		offsets: make([]int64, 8), deadtimes: make([]uint64, 8),
		window: uint64(5E-9 / am.ClkPeriod), lastTimestamp: make([]uint64, 8),
	}
	return &b, nil
}

// nexTag will return the next tag AFTER applying deadtimes and/or offsets, always
func (b *BinFile) nextTag() (Tagger, error) {
	for {
		tmp := make([]byte, b.ModeInfo.TagByteSize)
		n, ok := b.tagReader.Read(tmp)

		if n != b.ModeInfo.TagByteSize && ok != io.EOF {
			return nil, fmt.Errorf("could not read enough bytes, missing %d", b.ModeInfo.TagByteSize-n)
		} else if ok == io.EOF {
			return nil, ok
		} else if ok != nil {
			return nil, errors.New("unknown error while parsing raw tag")
		}

		switch b.ModeInfo.Mode {
		case I64f:
			tag := NewTagI64f(NewRawTag(tmp))

			// Apply deadtimes
			if (tag.timestamp - b.lastTimestamp[tag.channel]) < b.deadtimes[tag.channel] {
				continue
			}
			b.lastTimestamp[tag.channel] = tag.timestamp

			if ofs := b.offsets[tag.channel]; math.Signbit(float64(ofs)) {
				tag.timestamp -= uint64(math.Abs(float64(ofs)))
			} else {
				tag.timestamp += uint64(ofs)
			}

			return tag, nil
		default:
			return nil, errors.New("acquisition mode not implemented/recognised")
		}
	}
}

// SetTrigger sets the trigger channel used in the automatic offsets recognition
func (b *BinFile) SetTrigger(t uint8) error {
	if 0 < t && t < 9 {
		b.triggerCh = t - 1
		return nil
	}
	return ErrInvalidChannel
}

func (b *BinFile) Trigger() uint8 {
	return b.triggerCh
}

// SetDeadtimes sets the given deadtimes, transforming them from seconds to clock-ticks
func (b *BinFile) SetDeadtimes(deadtimes ...float64) {
	for i, deadtime := range deadtimes {
		b.deadtimes[i] = uint64(deadtime / b.ModeInfo.ClkPeriod)
	}
}

// Deadtimes returns the currently set deadtimes in seconds
func (b *BinFile) Deadtimes() []float64 {
	tmp := make([]float64, 8)
	for i, deadtime := range b.deadtimes {
		tmp[i] = float64(deadtime) * b.ModeInfo.ClkPeriod
	}
	return tmp
}

// SetOffsets accepts a list of offsets, in seconds, that will be applied sequentially (i.e. the
// the first argument will be the frist offset and so on)
func (b *BinFile) SetOffsets(offsets ...float64) {
	for i, ofs := range offsets {
		if i > 7 {
			break
		}
		b.offsets[i] = int64(ofs / b.ModeInfo.ClkPeriod)
	}
}

// SetOffset applies a single offest, in seconds, to a given channel
func (b *BinFile) SetOffset(ofs float64, ch uint8) {
	if 0 < ch && ch < 9 {
		b.offsets[ch-1] = int64(ofs / b.ModeInfo.ClkPeriod)
	}
}

// Offsets returns the currently set offsets, in seconds
func (b *BinFile) Offsets() []float64 {
	offsetsSeconds := make([]float64, 8)
	for i, ofs := range b.offsets {
		offsetsSeconds[i] = float64(ofs) * b.ModeInfo.ClkPeriod
	}
	return offsetsSeconds
}

// SetWindow sets the coincidence window width. The argument must be in seconds.
func (b *BinFile) SetWindow(width float64) {
	b.window = uint64(width / b.ModeInfo.ClkPeriod)
}

// Window returns the currently set coincidence window in seconds
func (b *BinFile) Window() float64 {
	return float64(b.window) * b.ModeInfo.ClkPeriod
}

// Close closes the underlying file reference
func (b *BinFile) Close() error {
	return b.fileRef.Close()
}
