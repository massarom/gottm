package gottm

import (
	"bufio"
	"bytes"
	"testing"
)

const testTagUint64 = uint64(0x0F00)

var testTag = []byte{
	byte((testTagUint64 >> (0 * 8)) & 0xFF), byte((testTagUint64 >> (1 * 8)) & 0xFF), byte((testTagUint64 >> (2 * 8)) & 0xFF), byte((testTagUint64 >> (3 * 8)) & 0xFF),
	byte((testTagUint64 >> (4 * 8)) & 0xFF), byte((testTagUint64 >> (5 * 8)) & 0xFF), byte((testTagUint64 >> (6 * 8)) & 0xFF), byte((testTagUint64 >> (7 * 8)) & 0xFF),
}

func TestOffsets(t *testing.T) {
	modInf := &AcquisitionModeInfo{
		ClkPeriod:   ClkPeriodI64f,
		Mode:        I64f,
		TagByteSize: 8}
	rd := bytes.NewReader(testTag)
	bufrd := bufio.NewReader(rd)
	b := BinFile{ModeInfo: modInf, tagReader: bufrd, offsets: make([]int64, 8),
		lastTimestamp: make([]uint64, 8), deadtimes: make([]uint64, 8)}

	b.SetOffset(-69.93e-9, 1)
	if tag, ok := b.nextTag(); ok != nil {
		t.Error("Could not read next tag")
	} else if ts := tag.Timestamp(); ts != 3000 {
		t.Errorf("Expecting timestamp %d, got %d", 3000, ts)
	}

}
