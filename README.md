gottm
=====

`gottm` is a series of programs aimed at analyzing binary files produced by the [AIT TTM8000](https://www.ait.ac.at/en/research-fields/physical-layer-security/optical-quantum-technologies/products-and-instrumentation/high-precision-time-resolution/)
module.

It's a small project thanks to which I aim to gain a better understand of `go` and its features.