package gottm

import (
	"fmt"
	"io"
	"sort"

	log "github.com/sirupsen/logrus"
)

// BitMask is a utility type for dealing with the coincidence algorithm
type BitMask uint8

// Set sets a specified bit in a bitmask
func (b *BitMask) Set(i ...uint8) {
	for _, bit := range i {
		if 0 < bit && bit < 9 {
			*b |= BitMask(uint8((1 << (bit - 1))))
		}
	}
}

// Clear clears a specified bit in a bitmask
func (b *BitMask) Clear(i ...uint8) {
	for _, bit := range i {
		if 0 < bit && bit < 9 {
			*b &^= BitMask(uint8(1 << (bit - 1)))
		}
	}
}

// ClearAll clreas all bits, setting everything to 0
func (b *BitMask) ClearAll() {
	*b = 0
}

func (b *BitMask) BitIsSet(bit uint8) bool {
	return (((b.Uint8() & (1 << (bit - 1))) >> (bit - 1)) & 1) == 1
}

// Equals implements the equality operation on BitMaps, since custom types cannot be compared using
// the standard == operator
func (b *BitMask) Equals(m BitMask) bool {
	return b.Uint8() == m.Uint8()
}

// Uint8 returns the BitMask as a plain unsigned 8-bit integer
func (b *BitMask) Uint8() uint8 {
	return uint8(*b)
}

func (b BitMask) String() string {
	return fmt.Sprintf("%b", b.Uint8())
}

// Singles returns all the single counts per channel in the file
func (b *BinFile) Singles() ([]uint64, error) {
	tagsRead := 0
	singles := make([]uint64, 8)
counting_loop:
	for {
		tag, ok := b.nextTag()
		tagsRead++
		if ok == io.EOF {
			break counting_loop
		} else if ok != nil {
			log.WithField("tagsRead", tagsRead).Error("Error before EOF")
			return singles, ok
		}
		singles[tag.Channel()-1]++
	}
	return singles, nil
}

// Coincidences returns the coincidente event among the listed channels, using the deadtimes and the
// offsets stored into the struct fields Deadtimes and Offsets (if any). Any channel outside the
// range [1..8] will be ignored silently.
func (b *BinFile) Coincidences(channels ...uint8) (coincidences uint64) {
	var inputBitMask BitMask
	var currentBitMask BitMask
	var tagsInWindow []Tagger

	for _, ch := range channels {
		inputBitMask.Set(ch)
	}
	for {
		tag, ok := b.nextTag()

		if ok == io.EOF {
			break
		} else if ok != nil {
			log.WithError(ok).WithField("file", b.fileRef.Name()).Fatal("Could not read next tag")
		}

		// Continue immediately if we don't care about this tag
		if !inputBitMask.BitIsSet(tag.Channel()) {
			continue
		}

		// Sort the tags in the window, since after applying offsets weird stuff might happen
		tagsInWindow = append(tagsInWindow, tag)
		sort.Sort(ByTimestamp(tagsInWindow))

		for {
			if tagsInWindow[len(tagsInWindow)-1].Timestamp()-tagsInWindow[0].Timestamp() > b.window {
				currentBitMask.Clear(tagsInWindow[0].Channel())

				// Slicing and reassigning does not free memory, so we have to copy stuff manually
				tmp := make([]Tagger, len(tagsInWindow)-1)
				copy(tmp, tagsInWindow[1:])
				tagsInWindow = tmp
			} else {
				break
			}
		}
		currentBitMask.Set(tag.Channel())
		if inputBitMask.Equals(currentBitMask & inputBitMask) {
			currentBitMask.ClearAll()
			coincidences++
		}
	}
	return coincidences
}
