package gottm

import (
	"errors"
	"io"
)

// AcquisitionMode describes the data format used when the binary file was created
type AcquisitionMode int

const (
	// Unknown acquisition mode, signals an error
	Unknown AcquisitionMode = -1
	// I64f means I-mode, 64bits, flat
	I64f = 0
	// I64p means I-mode, 64bits, packed
	I64p = 1
)

const (
	// ClkPeriodI64f is the distance, in seconds, between two consecutive ticks of the internal
	// clock
	ClkPeriodI64f float64 = 83.25E-12
)

const (
	// TagByteSizeI64f is th size of a single tag in I64F mode
	TagByteSizeI64f int = 8
)

const headerSize = 30
const acqModePos = 16 // TODO: find the correct one!

func (a AcquisitionMode) String() string {
	switch a {
	case I64f:
		return "I-Mode, 64bit, flat"
	case I64p:
		return "I-Mode, 64bit, packed"
	default:
		return "Unknown"
	}
}

// AcquisitionModeInfo hold the information about the acquisition mode of the current binary file
type AcquisitionModeInfo struct {
	Mode        AcquisitionMode
	ClkPeriod   float64
	TagByteSize int
}

// NewAcquisitionModeInfo parses the header from a binary file and returns a pointer to the info
// struct.
func NewAcquisitionModeInfo(r io.Reader) (*AcquisitionModeInfo, error) {
	am := new(AcquisitionModeInfo)
	header := make([]byte, headerSize)
	n, ok := r.Read(header)
	if ok != nil {
		return am, errors.New("could not read the header, unknown reason")
	} else if n != headerSize {
		return am, errors.New("could not read the header, got too few bytes")
	}
	switch AcquisitionMode(header[acqModePos]) {
	case I64f:
		am.ClkPeriod = ClkPeriodI64f
		am.Mode = I64f
		am.TagByteSize = 8
		return am, nil
	default:
		return am, errors.New("unknown or not-implemented acquisition mode")
	}
}
