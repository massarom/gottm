package gottm

import (
	"encoding/binary"
	"fmt"
)

// RawTag Type to hold the raw tag extracted directly from a binary file
type RawTag struct {
	data []byte
}

// NewRawTag creates a new raw tag from a byte string
func NewRawTag(b []byte) *RawTag {
	return &RawTag{data: b}
}

// Uint64 returns the tag as unsigne 64 bits integer
func (r *RawTag) Uint64() uint64 {
	return binary.LittleEndian.Uint64(r.data)
}

// Bytes returns a copy of the underlying byte slice
func (r *RawTag) Bytes() []byte {
	return r.data
}

// Tagger describes a general time tag with a channel, slope and timestamp
type Tagger interface {
	Channel() uint8
	Slope() uint8
	Timestamp() uint64
}

// ByTimestamp implements the sort.Interface so that parsed tags can be sorted easily
type ByTimestamp []Tagger

func (b ByTimestamp) Len() int           { return len(b) }
func (b ByTimestamp) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }
func (b ByTimestamp) Less(i, j int) bool { return b[i].Timestamp() < b[j].Timestamp() }

// TagI64f I-Mode 64bit tag, flat
type TagI64f struct {
	channel   uint8
	slope     uint8
	timestamp uint64
}

// NewTagI64f creates a new I64F tag from a raw one
func NewTagI64f(r *RawTag) *TagI64f {
	t := TagI64f{
		channel:   uint8(r.Uint64() >> 61),
		slope:     uint8(r.Uint64() >> 60 & 1),
		timestamp: r.Uint64() & 0x00FFFFFFFFFFFFFF}
	return &t
}

// Channel returns the channel of the tag, NOT 0-based
func (t *TagI64f) Channel() uint8 {
	return t.channel + 1
}

// Slope returns the edge on which the event fired. 1: rising, 0: falling
func (t *TagI64f) Slope() uint8 {
	return t.slope
}

// Timestamp returns the timestamp of the tag in seconds
func (t *TagI64f) Timestamp() uint64 {
	return t.timestamp
}

func (t *TagI64f) String() string {
	return fmt.Sprintf("%d|%d|%d", t.channel, t.slope, t.timestamp)
}
