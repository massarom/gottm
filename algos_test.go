package gottm_test

import (
	"encoding/base64"
	"encoding/binary"
	"io/ioutil"
	"os"
	"testing"

	log "github.com/sirupsen/logrus"

	"bitbucket.org/massarom/gottm"
)

func TestBitMask(t *testing.T) {
	var bm gottm.BitMask = 5
	var bm2 gottm.BitMask = 5

	if i := bm.Uint8(); i != 5 {
		t.Errorf("Expecting 5, got %d", i)
	}

	if !bm.Equals(bm2) {
		t.Error("Equality between BitMasks failed")
	}
	if !bm.Equals(bm) {
		t.Errorf("Equality with itself failed. The deferenced pointer value is %d", (&bm).Uint8())
	}

	bm.ClearAll()
	if i := bm.Uint8(); i != 0 {
		t.Errorf("Expecting 0, got %d", i)
	}

	bm.Set(1, 5, 4)
	comp := uint8(1<<0 | 1<<4 | 1<<3)
	if i := bm.Uint8(); i != comp {
		t.Errorf("Expecting %d, got %d", comp, i)
	}
	if !bm.BitIsSet(1) {
		t.Error("Bit 1 not set")
	}

	bm.Clear(5, 1)
	comp &^= uint8(1<<4 | 1<<0)
	if i := bm.Uint8(); i != comp {
		t.Errorf("Expecting %d, got %d", comp, i)
	}

}

func makeDummyBinFile() string {
	FakeHeaderI64 := []byte("VFRNOERhdGEBBLXUgQAAAAAAiAC0HwAAAAAYJ/j/")
	fr, ok := ioutil.TempFile(os.Getenv("TMP"), "gottm-bintest")
	defer fr.Close()
	defer log.Infof("Tempfile at %s\n", fr.Name())
	if ok != nil {
		panic(ok)
	}
	hdr := make([]byte, 30)
	n, ok := base64.StdEncoding.Decode(hdr, FakeHeaderI64)
	if ok != nil {
		log.Fatal("Couldn't decode the fake header")
	} else if n != 30 {
		log.Fatalf("Didn't decode the whole header, got %d bytes instead of 30", n)
	}
	fr.Write(hdr)

	// We will write a binary file with a total of 100 tags, 60 in channel 1 and 40 in channel 2.
	// The first 40 tags in channel 1 will have the same timestamp of those in channel 2, so we will
	// have 40 coincidences.

	for i := uint64(0); i < 40; i++ {
		tag := uint64(0)

		tag |= 1 << 60
		tag |= i
		ok := binary.Write(fr, binary.LittleEndian, tag)
		if ok != nil {
			log.Fatal("Couldn't write tag to file")
		}

		tag = 0
		tag |= 1 << 61
		tag |= 1 << 60
		tag |= i
		ok = binary.Write(fr, binary.LittleEndian, tag)
		if ok != nil {
			log.Fatal("Couldn't write tag to file")
		}
	}

	for i := uint64(40); i < 60; i++ {
		tag := uint64(0)

		tag |= 1 << 60
		tag |= i
		ok := binary.Write(fr, binary.LittleEndian, tag)
		if ok != nil {
			log.Fatal("Couldn't write tag to file")
		}
	}

	return fr.Name()
}

func TestSingles(t *testing.T) {
	fname := makeDummyBinFile()
	bin, ok := gottm.NewBinFile(fname)
	if ok != nil {
		t.Errorf("Could not open temp bin-file, reason was: %s", ok.Error())
	}
	defer bin.Close()
	defer os.Remove(fname)
	if bin.ModeInfo.Mode != gottm.I64f {
		t.Errorf("Expecting I64f. got %s", bin.ModeInfo.Mode.String())
	}

	singles, _ := bin.Singles()
	if singles[0] != 60 {
		t.Errorf("Expecting 60 counts, got %d", singles[0])
	}
	if singles[1] != 40 {
		t.Errorf("Expecting 40 counts, got %d", singles[1])
	}
}

func TestCoincidences(t *testing.T) {
	fname := makeDummyBinFile()
	bin, ok := gottm.NewBinFile(fname)
	if ok != nil {
		t.Errorf("Could not open temp bin-file, reason was: %s", ok.Error())
	}
	defer bin.Close()
	defer os.Remove(fname)
	if bin.ModeInfo.Mode != gottm.I64f {
		t.Errorf("Expecting I64f. got %s", bin.ModeInfo.Mode.String())
	}
	bin.SetWindow(bin.ModeInfo.ClkPeriod)

	c := bin.Coincidences(1, 2)
	if c != 40 {
		t.Errorf("Expecting 40 coincidences, got %d", c)
	}

}
